# -*- coding: utf-8 -*-
"""
Created on Tue Apr  9 22:35:13 2019

@author: Sergi
"""

import os
import sys
sys.path.insert(0, './libs')    #Agregar ruta al Path de Python
from cnn_utils_ex import *

############################ INICIALIZACION ###############################
###########################################################################
parent_dir='.'
#imagenes
img_dir = parent_dir + '/images'
file_ext='*.png'
#Dataset
dataset_dir = parent_dir + '/dataset'
#training_dir
saved_training_dir = parent_dir + '/savedTraining'
#Segmentation dir
segmentated_img_dir = parent_dir + '/segmentated_img'
      
datasetTrainFile = dataset_dir + '/train_Dataset.h5'
datasetTestFile = dataset_dir + '/test_Dataset.h5'

classes_id={
        "Parasitized":0,
        "Uninfected":1
        }
num_classes  = len(classes_id)

max_train_samples_count = 6500 
max_test_samples_count = 5000 
image_size = (64,64)
epochs = 7

########################### CREAR DIRECTORIOS ##############################
############################################################################
directories = [img_dir,dataset_dir,saved_training_dir,segmentated_img_dir]
for directory in directories:
    try:
        os.mkdir(directory)
    except OSError:
        print("Falla al crear directorio " + directory)
    else:
        print("Directorio " + directory + " creado correctamente")