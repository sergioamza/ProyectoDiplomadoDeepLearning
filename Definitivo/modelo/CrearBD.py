# -*- coding: utf-8 -*-
"""
Created on Sun Mar 17 15:10:49 2019
Malaria
@author: Sergi
"""

import sys
sys.path.insert(0, './libs')    #Agregar ruta al Path de Python
from cnn_utils_ex import *

############################ INICIALIZACION ###############################
###########################################################################
from DatosGenerales import *

####################CREACION DE LA BASE DE DATOS###########################
###########################################################################
#Se carga la informacion de las imagenes



features,labels = extract_features(img_dir, file_ext, classes_id, 0)##
save_dataset(features,labels,datasetTrainFile,datasetTestFile)
