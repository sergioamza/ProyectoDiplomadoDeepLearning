# -*- coding: utf-8 -*-
"""
Created on Thu Apr 18 17:21:10 2019

@author: Sergi
"""

#Extract contours from images

import cv2
import numpy as np

import sys
sys.path.insert(0, './libs')    #Agregar ruta al Path de Python

from DatosGenerales import *
import easygui

debug = False

################# INTERACCION CON RATON #####################
#############################################################
point1 = (0,0)
point2 = (0,0)
drawing = False
done = False

def mouseDrawing(event, x, y, flags, params):
    global point1, point2, drawing, done
    
    if event == cv2.EVENT_LBUTTONDOWN:    
        drawing = True
        done = False
        point1 = (x, y)
        if debug:
            print("Mouse pressed")
    
    elif event == cv2.EVENT_MOUSEMOVE:
        if drawing is True:
            point2 = (x, y)
            if debug:
                print("Mouse move")
    
    elif event == cv2.EVENT_LBUTTONUP:
        if debug:
            print("Mouse released")
            print("P1 "+str(point1)+" P2 "+str(point2))
            print("W "+str(abs(point1[0]-point2[0]))+" H "+str(abs(point1[1]-point2[1])))
        drawing = False
        done = True
        
cv2.namedWindow("Frame")
cv2.setMouseCallback("Frame", mouseDrawing)
#############################################################


################# SEGMENTACION IMAGENES #####################
#############################################################
def segmentImage(img, idx = 0, stk = 0):
    stk = stk + 1

    if debug:
        cv2.imshow('Original',img)
#         cv2.waitKey(0)
    
    #CONVERTIR A ESCALA DE GRISES
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    if debug:
        cv2.imshow('Gray',gray)
#    cv2.waitKey(0)
    
    #APLICAR UMBRAL
    if idx==1:           
        blurGray = cv2.medianBlur(gray,3)
        thresh = cv2.adaptiveThreshold(blurGray,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY_INV,31,2)
    elif idx==2:           
        blurGray = cv2.medianBlur(gray,5)
        thresh = cv2.adaptiveThreshold(blurGray,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY_INV,21,2)
    else:
        blurGray = cv2.medianBlur(gray,7 )
        thresh = cv2.adaptiveThreshold(blurGray,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY_INV,41,1)
    if debug:
        cv2.imshow('Threshold',blurGray)
#    cv2.waitKey(0)
    
    #BUSCAR Y ENCONTRAR CONTORNOS
    contourImg, contours, hierarchy = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    if debug:
        cv2.imshow('Contours',contourImg)
        cv2.waitKey(0)
    
    if debug:
        print("Num of shapes {0}".format(len(contours)))
    img_name = "img"
    
    #PROCESAR, SELECCIONAR Y GUARDAR CONTORNOS COMO IMAGENES
    ok = 0
    nok = 0
    img_cont = img.copy()
    for cnt in contours:
        rect=cv2.minAreaRect(cnt)
        ((x, y), (w, h), angle) = rect
        if w > maxCellProp * cellSizePx and h > minCellProp * cellSizePx or w > minCellProp * cellSizePx and h > maxCellProp * cellSizePx:
            box = cv2.boxPoints(rect)
            box = np.int0(box)
            img_cont = cv2.drawContours(img_cont, [box], 0, (0,255,0))
            img_crop = crop_minAreaRect(img, rect)
            w = np.size(img_crop,0)
            h = np.size(img_crop,1)
            if (w > cellSizePx  or h > cellSizePx) and stk < 1  : #STK cuantas veces se puede tratar de separar subimágenes
                cv2.destroyAllWindows()
                idx = segmentImage(img_crop, idx, stk)
#                cv2.waitKey(0)
                if debug:
                    print("Imagen "+str(ok)+" con tamaño "+str(w)+"x"+str(h))
            else:
                height = np.size(img_crop, 0)
                width = np.size(img_crop, 1)
                try:
                    img_out_name = segmentated_img_dir+"/"+img_name+"_part_"+str(idx)+"_"+str(stk)+".png"
                    if debug:
                        cv2.imshow('Cropped'+str(ok),img_crop)
                        print("Guardando" + img_out_name)
                    if width > 0 and height >0:
                        cv2.imwrite(img_out_name,img_crop)
                    if debug:
                        print("Guardado")
                    ok = ok + 1
                    idx = idx + 1
                except:
                    nok = nok + 1
        else:
            np.delete(contours, cnt)
     
    if debug:
        print("Ok: " + str(ok) + " Nok: " + str(nok))
    
    if debug:
        cv2.imshow('Contour',img_cont)
#    cv2.waitKey(0)
    return idx

# CONVIERTE UN AREA EN IMAGEN
def crop_minAreaRect(img, rect):

#    rect = cv2.minAreaRect(cnt)
    ((x, y), (W, H), angle) = rect
    box = cv2.boxPoints(rect) 
    box = np.int0(box)
    
#    W = rect[1][0]
#    H = rect[1][1]
    
    Xs = [i[0] for i in box]
    Ys = [i[1] for i in box]
    x1 = min(Xs)
    x2 = max(Xs)
    y1 = min(Ys)
    y2 = max(Ys)
    
    clockwise = 1
    angle = rect[2]
    if angle < -45:
        angle += 90
        clockwise = 0
    
    # Center of rectangle in source image
    center = ((x1+x2)/2,(y1+y2)/2)
    # Size of the upright rectangle bounding the rotated rectangle
    size = (x2-x1, y2-y1)
    M = cv2.getRotationMatrix2D((size[0]/2, size[1]/2), angle, 1.0)
    # Cropped upright rectangle
    cropped = cv2.getRectSubPix(img, size, center)
    cropped = cv2.warpAffine(cropped, M, size)
    croppedW = H if H > W else W
    croppedH = H if H < W else W
    if clockwise == 1:
        croppedW = W
        croppedH = H
    else:
        croppedW = H
        croppedH = W        
    # Final cropped & rotated rectangle
    croppedRotated = cv2.getRectSubPix(cropped, (int(croppedW),int(croppedH)), (size[0]/2, size[1]/2))
    return croppedRotated

#############################################################################################
######################################### EJECUCION #########################################
#############################################################################################
    

#Abrir imagen
img_path = easygui.fileopenbox()
img = cv2.imread(img_path)
height = np.size(img, 0)
width = np.size(img, 1)
scale = (height/640) 
img = cv2.resize(img, ((int)(width/scale), (int)(height/scale)), interpolation=cv2.INTER_CUBIC)#

## CICLO PARA LA SELECCION DEL TAMAÑO DE IMAGEN ##
#TODO Seleccionar la mas grande y la mas pequeña
while True:
   #_, frame = cap.read()
   frame = img.copy()
   if drawing:
      cv2.rectangle(frame,point1,point2,(0,0,255),0)
      cv2.putText(frame,"W "+str(abs(point1[0]-point2[0]))+" H "+str(abs(point1[1]-point2[1])),point1,
      cv2.FONT_HERSHEY_DUPLEX,.5,(255,0,0),1)

   if not done:
      cv2.imshow("Frame", frame)
   key = cv2.waitKey(25)
#   print("Key: "+str(key))
   if key== 13:    
     print('done')
     done = False
     break
   elif key == 27:
     break

# Asignar valores de tamaños de celulas

cellSizePx = 0
maxCellProp = 0.8
minCellProp = 0.5

W = abs(point1[0]-point2[0])
H = abs(point1[1]-point2[1])

if H < W:
    cellSizePx = W
else:
    cellSizePx =H

#cellSizePx = (W+H)/2

#Segmentar imagenes
    
idx = segmentImage(img, 0, 0)
infectada = 0
sana = 0
###################################################################################################
########################## CLASIFICAR LAS IMAGENES CON EL MODELO ENTRENADO ########################

from os import walk
from cnn_utils_ex import *
from cnn_libs import *

img_predecir = []
for (dirpath, dirnames, filenames) in walk(segmentated_img_dir):
    img_predecir.extend(filenames)
    print(filenames)
    break
infectada = 0
sana = 0
for img_path in img_predecir:        
    ################### CARGA DE DATOS PARA PREDICCION ########################
    ###########################################################################
    
    if debug:
        print("##################################################################################################")
        print("Prediccion para la imagen " + img_path)
    img_pred = cv2.imread(segmentated_img_dir+"/"+img_path)
    img_pred = cv2.resize(img_pred, (image_size[0],image_size[1]), interpolation=cv2.INTER_CUBIC)#
    
    #plt.show(img)
    img_test = img_pred/255.
    img_test=img_test[np.newaxis,...]
    
    y_label= np.array([2])
    y_label = convert_to_one_hot(y_label, 6).T
    
    ############################ PREDICCION ###################################
    ###########################################################################
    prediccion = model_predict(img_test, y_label)
    if prediccion == [1]:
        sana = sana + 1
#        cv2.imwrite("./out/sanas/"+img_path,img)
    if prediccion == [0]:
        infectada = infectada + 1
#        cv2.imwrite("./out/infectadas/"+img_path,img)
    if debug:
        print("La imagen " + img_path + " es posiblemente: ", prediccion)
        print("--------------------------------------------------------------------------------------------------")
print("Infectadas " + str(infectada) + " Sanas " + str(sana) + " Afectacion: " + str(100*infectada/(infectada+sana)) + "%")

###################################################################################################
cv2.putText(img,"Conteo: "+str(infectada+sana),(5,20),cv2.FONT_HERSHEY_DUPLEX,.65,(255,0,0),2)
cv2.putText(img,"Infectadas: "+str(infectada)+' ({:0.1f}%)'.format(100*infectada/(infectada+sana)),(5,40),cv2.FONT_HERSHEY_DUPLEX,.65,(255,0,0),2)
cv2.putText(img,"Sanas: "+str(sana)+' ({:0.1f}%)'.format(100*sana/(infectada+sana)),(5,60),cv2.FONT_HERSHEY_DUPLEX,.65,(255,0,0),2)
cv2.imshow("Counting", img)
#TODO Las imagenes se deben guardar por el nombre de la imagen orignen o por una secuencia
cv2.imwrite(segmentated_img_dir+"/"+"result.png",img)
cv2.waitKey(0)

cv2.destroyAllWindows()