#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 11 12:50:13 2018

@author: madcas
"""

import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from scipy import ndimage
import tensorflow as tf
from tensorflow.python.framework import ops
import sys
sys.path.insert(0, './libs')    #Agregar ruta al Path de Python
from cnn_utils_ex import *
from cnn_libs import *
import cv2


############################ INICIALIZACION ###############################
###########################################################################
from DatosGenerales import *

img_predecir = [img_dir+"/Parasitized/C189P150ThinF_IMG_20151203_142224_cell_98.png",
              img_dir+"/Uninfected/C241NThinF_IMG_20151207_124643_cell_169.png"]

from os import walk

img_predecir = []
for (dirpath, dirnames, filenames) in walk(img_dir+"/Parasitized"):
    img_predecir.extend(filenames)
    print(filenames)
    break
infectada = 0
sana = 0
for img_path in img_predecir:
        
    ################### CARGA DE DATOS PARA PREDICCION ########################
    ###########################################################################
    print("##################################################################################################")
    print("Prediccion para la imagen " + img_path)
    img = cv2.imread(img_dir+"/Parasitized/"+img_path)
    img = cv2.resize(img, (64, 64), interpolation=cv2.INTER_CUBIC)#
    
    #plt.show(img)
    img_test = img/255.
#    img_test = np.divide(img,255)
    img_test=img_test[np.newaxis,...]
    
    y_label= np.array([2])
    y_label = convert_to_one_hot(y_label, 6).T
    
    ############################ PREDICCION ###################################
    ###########################################################################
    prediccion = model_predict(img_test, y_label)
    if prediccion == [1]:
        sana = sana + 1
        cv2.imwrite("./out/sanas/"+img_path,img)
    if prediccion == [0]:
        infectada = infectada + 1
        cv2.imwrite("./out/infectadas/"+img_path,img)
    print("La imagen " + img_path + " es posiblemente: ", prediccion)
    print("--------------------------------------------------------------------------------------------------")
if(infectada+sana)>0:
    print("Infectadas " + str(infectada) + " Sanas " + str(sana) + " Afectacion: " + str(100*infectada/(infectada+sana)) + "%")


