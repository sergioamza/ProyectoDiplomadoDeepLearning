# -*- coding: utf-8 -*-
"""
Created on Tue Apr  9 21:26:18 2019

@author: Sergi
"""

#import matplotlib.pyplot as plt
from PIL import Image
from scipy import ndimage
import tensorflow as tf
from tensorflow.python.framework import ops
import numpy as np
import sys
sys.path.insert(0, './libs')    #Agregar ruta al Path de Python
from cnn_utils_ex import *
from cnn_libs import *


############################ INICIALIZACION ###############################
###########################################################################
from DatosGenerales import *
print("Inicializacion completa")
#######################CARGA DE LA BASE DE DATOS###########################
###########################################################################
print("Cargando la base de datos")
X_train_orig, Y_train_orig, X_test_orig, Y_test_orig, classes = load_dataset(parent_dir,datasetTrainFile,datasetTestFile)      # Cargar la base de datos
print("Redimensionando la base de datos a los tamaños establecidos")
print ("Cantidad de muestras de la BD Entrenamiento: {:d}, Prueba: {:d}".format(len(X_train_orig),len(X_test_orig)))
print ("Cantidad de muestras deseadas Entrenamiento: {:d}, Prueba: {:d}".format(max_train_samples_count,max_test_samples_count))
if (max_train_samples_count > 0 ) and (max_train_samples_count < len(X_train_orig)) :
    X_train_orig = X_train_orig[:max_train_samples_count]
    Y_train_orig = Y_train_orig[:,:max_train_samples_count]
if (max_test_samples_count > 0 ) and (max_test_samples_count < len(X_train_orig)) :
    X_test_orig = X_test_orig[:max_test_samples_count]
    Y_test_orig = Y_test_orig[:,:max_test_samples_count]
print ("Cantidad de muestras Final Entrenamiento: {:d}, Prueba: {:d}".format(len(X_train_orig),len(X_test_orig)))
print("Carga completa")

#
#index = 20
#plt.figure(1)
#plt.imshow(X_train_orig[index])
#print ("y = " + str(np.squeeze(Y_train_orig[:, index])))


#################DEFINICION E INCIALIZACION DEL MODELO#####################
###########################################################################

print("Adecuando datos de entrada")

X_train = X_train_orig/255.
X_test = X_test_orig/255.


Y_train = convert_to_one_hot(Y_train_orig, num_classes).T
Y_test = convert_to_one_hot(Y_test_orig, num_classes).T

del X_train_orig, X_test_orig, Y_train_orig, Y_test_orig
print("Adecuacion completa")

print("Obteniendo modelo")
_, _, parameters = model(X_train, Y_train, X_test, Y_test, 0.009, epochs)
print("Modelo completo")
