# -*- coding: utf-8 -*-
"""
Created on Sun Mar 17 13:08:07 2019

@author: Sergio Martinez
"""

import glob
import os
import math
import numpy as np
import h5py
from PIL import Image
from sklearn.utils import shuffle


############################ INICIALIZACION ###############################
###########################################################################
from DatosGenerales import *
print("Inicializacion completa")
#-------------------------------------------------------------------------#

def load_dataset(parent_dir,datasetTrainFile,datasetTestFile):
    print("datasetTrainFile " + datasetTrainFile + "datasetTestFile " + datasetTestFile)
    train_dataset = h5py.File(os.path.join(parent_dir, datasetTrainFile), "r")
    train_set_x_orig = np.array(train_dataset["train_set_x"][:]) # your train set features
    train_set_y_orig = np.array(train_dataset["train_set_y"][:]) # your train set labels

    test_dataset = h5py.File(os.path.join(parent_dir, datasetTestFile), "r")
    test_set_x_orig = np.array(test_dataset["test_set_x"][:]) # your test set features
    test_set_y_orig = np.array(test_dataset["test_set_y"][:]) # your test set labels
    
    classes = []
#    classes = np.array(test_dataset["list_classes"][:]) # the list of classes
    
    train_set_y_orig = train_set_y_orig.reshape((1, train_set_y_orig.shape[0]))
    test_set_y_orig = test_set_y_orig.reshape((1, test_set_y_orig.shape[0]))
    
    return train_set_x_orig, train_set_y_orig, test_set_x_orig, test_set_y_orig, classes



def extract_features(parent_dir,file_ext, classes_id, limite = 0):
    imgs = []
    labels = []
    for sub_dir in os.listdir(parent_dir):
        print(sub_dir)
        attempt = 0
        ok = 0
        nok = 0
        for fn in glob.glob(os.path.join(parent_dir, sub_dir, file_ext)):
          #print(fn)
          if (attempt==0 or attempt<limite or limite == 0):
              try:
                img = Image.open(fn)
                attempt+=1
                if (img.size[0] >= 50):
                  etiqueta=label2num(sub_dir, classes_id)
    #              print("Etiqueta OK")
                  img=img.resize(image_size,Image.ANTIALIAS)
    #              print("Resize OK")
                  im=np.array(img)
                  if im.shape[2]==3:
                    imgs.append(im)
                    labels.append(etiqueta)             
                    ok+=1
                  else:
                    print(fn,im.shape) 
              except:
                nok+=1
                print('Error',fn)
        print("Attempts: " + str(attempt) +" OK: " + str(ok) + " Nok: " + str(nok))
    features = np.asarray(imgs).reshape(len(imgs),image_size[0],image_size[1],3)
    return features, np.array(labels,dtype = np.int)

def save_dataset(features, labels, datasetTrainFile, datasetTestFile):

    # Si se requiere se hace alguno de los siguientes procesos.
    # Mesclar aleatoriamente la base de datos
    x1, y1 = shuffle(features, labels)

    # O Separarla en entrenamieto y prueba (o si es necesario en validación)
    samples=y1.size
    y1=y1.reshape((samples,1))

    offset = int(x1.shape[0] * 0.80)
    X_train, Y_train = x1[:offset], y1[:offset]
    X_test, Y_test = x1[offset:], y1[offset:]
    Y_test = np.array(Y_test)
    Y_train = np.array(Y_train)
    # Se puede adicionar el proceso que requieran para su base de datos
    
    # Este sería el proceso para guardar en un formato h5 los datos
    with h5py.File(datasetTrainFile,'w') as h5data:
        h5data.create_dataset('train_set_x',data=X_train)
        h5data.create_dataset('train_set_y',data=Y_train)
    with h5py.File(datasetTestFile,'w') as h5data:
        h5data.create_dataset('test_set_x',data=X_test)
        h5data.create_dataset('test_set_y',data=Y_test)
        

def label2num(x, classes_id):
    if x in classes_id:
        return classes_id[x]
    else:
        return(21)
        

def random_mini_batches(X, Y, mini_batch_size = 10, seed = 0):
    """
    Creates a list of random minibatches from (X, Y)
    
    Arguments:
    X -- input data, of shape (input size, number of examples) (m, Hi, Wi, Ci)
    Y -- true "label" vector (containing 0 if cat, 1 if non-cat), of shape (1, number of examples) (m, n_y)
    mini_batch_size - size of the mini-batches, integer
    seed -- this is only for the purpose of grading, so that you're "random minibatches are the same as ours.
    
    Returns:
    mini_batches -- list of synchronous (mini_batch_X, mini_batch_Y)
    """
    
    m = X.shape[0]                  # number of training examples
    mini_batches = []
    np.random.seed(seed)
#    print(m)
    
    # Step 1: Shuffle (X, Y)
    permutation = list(np.random.permutation(m))
    shuffled_X = X[permutation,:,:,:]
    shuffled_Y = Y[permutation,:]

    # Step 2: Partition (shuffled_X, shuffled_Y). Minus the end case.
    num_complete_minibatches = math.floor(m/mini_batch_size) # number of mini batches of size mini_batch_size in your partitionning
    for k in range(0, num_complete_minibatches):
        mini_batch_X = shuffled_X[k * mini_batch_size : k * mini_batch_size + mini_batch_size,:,:,:]
        mini_batch_Y = shuffled_Y[k * mini_batch_size : k * mini_batch_size + mini_batch_size,:]
        mini_batch = (mini_batch_X, mini_batch_Y)
        mini_batches.append(mini_batch)
    
    # Handling the end case (last mini-batch < mini_batch_size)
    if m % mini_batch_size != 0:
        mini_batch_X = shuffled_X[num_complete_minibatches * mini_batch_size : m,:,:,:]
        mini_batch_Y = shuffled_Y[num_complete_minibatches * mini_batch_size : m,:]
        mini_batch = (mini_batch_X, mini_batch_Y)
        mini_batches.append(mini_batch)
    
    return mini_batches


def convert_to_one_hot(Y, C):
    Y = np.eye(C)[Y.reshape(-1)].T
    return Y
